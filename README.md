# My Home Assistant Config

I'll try to keep this readme updated. This is very much a work in progress.

## My Setup

- An old computer I had lying around.
- Docker running:
  - Home Assistant
  - Plex Media Server
  - Portainer
  - MariaDB
  - InfluxDB
  - Telegraf
  - Chronograf
  - Kapacitor
  - Mosquitto
  - Node-RED
  - Bookstack
- Additional software:
  - Nginx
  - Let's Encrypt (certbot)
- Aeon Labs Z Wave Stick (GEN 5)
- Philips Hue Hub

## Current Plans

- Migrate to Lovalace
- Create a Floorplan
- Set up a Fire Tablet as controller
- Add more smart lighting
- Add a Broadlink RM Pro to my setup
- More automations